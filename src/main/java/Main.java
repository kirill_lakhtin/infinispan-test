import org.infinispan.Cache;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;

import java.util.Random;

/**
 * @author Kirill Lakhtin.
 */
public class Main {

  private static final String PREFIX = "PREFIX";
  private static final Random random = new Random();

  public static void main(String[] args) {
    DefaultCacheManager cacheManager = setUpCacheManager();
    Cache<String, String> cache = cacheManager.getCache("replicated_cache");
    System.out.println("Clustering: " +cache.getCacheConfiguration().clustering().cacheModeString());

    System.out.println("Enter 'W' for write or 'R' for read all cache entries");
    java.util.Scanner scanner = new java.util.Scanner(System.in);
    while (true) {

      String next = scanner.next();
      switch (next) {

        case "W": {
          String nextValue = getUniqueValue();
          cache.put(nextValue, nextValue);
        }break;

        case "R": {
          cache.entrySet().forEach(System.out::println);
        }break;

        default: {
          System.out.println("Unknown command");
        }
      }
    }
  }

  private static DefaultCacheManager setUpCacheManager() {
    //set multicast as a transport
    final GlobalConfigurationBuilder builder = new GlobalConfigurationBuilder();
    final GlobalConfiguration globalConfiguration = builder.transport()
      .defaultTransport()
      .addProperty("configurationFile", "jgroups-config.xml")
      .clusterName("cluster")
      .build();

    //define replicated cache
    DefaultCacheManager cacheManager = new DefaultCacheManager(globalConfiguration);
    cacheManager.defineConfiguration("replicated_cache", new ConfigurationBuilder()
    .clustering()
    .cacheMode(CacheMode.REPL_SYNC)
      .clustering()
      .partitionHandling().enabled(true)
      .build());
    return cacheManager;
  }

  private static String getUniqueValue() {
    return PREFIX + random.nextInt(100);
  }
}
